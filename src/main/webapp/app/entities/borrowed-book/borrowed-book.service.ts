import axios from 'axios';

import buildPaginationQueryOpts from '@/shared/sort/sorts';

import { IBorrowedBook } from '@/shared/model/borrowed-book.model';

const baseApiUrl = 'api/borrowed-books';
const baseSearchApiUrl = 'api/_search/borrowed-books?query=';

export default class BorrowedBookService {
  public search(query, paginationQuery): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(`${baseSearchApiUrl}${query}&${buildPaginationQueryOpts(paginationQuery)}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public find(id: number): Promise<IBorrowedBook> {
    return new Promise<IBorrowedBook>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(paginationQuery?: any): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl + `?${buildPaginationQueryOpts(paginationQuery)}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IBorrowedBook): Promise<IBorrowedBook> {
    return new Promise<IBorrowedBook>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IBorrowedBook): Promise<IBorrowedBook> {
    return new Promise<IBorrowedBook>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
